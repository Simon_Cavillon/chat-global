# -*-coding:Latin-1 -*

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_socketio import SocketIO, send
from flask import Blueprint
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import LoginManager
import click
import os
basedir = os.path.abspath(os.path.dirname(__file__))

# init SQLAlchemy so we can use it later in our models
db = SQLAlchemy()


def create_app():
    app = Flask(__name__)

    app.config['SECRET_KEY'] = 'hard to guess string'
    app.config['SQLALCHEMY_DATABASE_URI'] =\
    'sqlite:///' + os.path.join(basedir, 'data.sqlite')
    app.config['SQLALCHEMY_COMMIT_ON_TEARDOWN'] = True
    app.config['TESTING'] = False


	# SocketIO extension
    socketIo = SocketIO(app)


    from .auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint)

    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    db.init_app(app)
    socketIo.run(app)
    login_manager = LoginManager()
    login_manager.login_view = 'auth.login'
    login_manager.init_app(app)

    def messageReceived(methods=['GET', 'POST']):
      print('message was received!!!')

    @socketIo.on('message')
    def handle_my_custom_event(msg):
      print('Message : '+msg)
      send(msg.encode('latin-1').decode('utf-8'), broadcast=True)
      return None

    from .models import UTILISATEUR
    @login_manager.user_loader
    def load_user(user_id):
        return UTILISATEUR.query.get(int(user_id))

    @app.cli.command("addutilisateur")
    @click.argument("email")
    @click.argument("password")
    @click.argument("nom")
    def addutilisateur(email, password, nom):
       new_utilisateur = UTILISATEUR(email=email, nom=nom, password=generate_password_hash(password, method='sha256'))
       db.session.add(new_utilisateur)
       db.session.commit()

    @app.cli.command("passwd")
    @click.argument('nom')
    @click.argument('passwd')
    def passwd(nom, passwd):
       utilisateur = UTILISATEUR.query.filter_by(nom=nom).first()
       if utilisateur:
         utilisateur.password = generate_password_hash(passwd, method='sha256')
         db.session.commit()
       else:
         print("****************************************************")
         print("Il n'y a pas de compte associé au nom " + nom)
         print("****************************************************")

    @app.cli.command()
    def syncdb():
       db.create_all(app=create_app())

    return app
