from flask_login import UserMixin
import datetime
from . import db

# Classe Parent
class UTILISATEUR(UserMixin, db.Model):
    __tablename__ = 'utilisateur'
    id = db.Column(db.Integer, primary_key=True) # Clé primaire
    email = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(100))
    dateCreation = db.Column(db.DateTime, default=datetime.datetime.now())
    nom = db.Column(db.String(1000))
