from flask_login import login_required, current_user
from flask_socketio import emit, ConnectionRefusedError, disconnect
from werkzeug.security import generate_password_hash, check_password_hash
from flask import Blueprint, render_template, redirect, url_for, request, flash
from .models import UTILISATEUR
from . import db
import socketio
import datetime
import locale

locale.setlocale(locale.LC_TIME,'')

main = Blueprint('main', __name__)

@main.route('/', methods=['GET', 'POST'])
@login_required
def index():
    return render_template('index.html', name=current_user.nom)

#############################
########  PROFILE  ##########
#############################

@main.route('/profile')
@login_required
def profile():
    return render_template('profile.html', name=current_user.nom, mail=current_user.email)

@main.route('/changePW', methods=['GET', 'POST'])
@login_required
def changePW():
    ancienMDP= request.form.get('old')
    nouveauMDP= request.form.get('new')

    user = UTILISATEUR.query.filter_by(id=current_user.id).first()

    if not user or check_password_hash(user.password, ancienMDP):
        current_user.password=generate_password_hash(nouveauMDP, method='sha256')
        db.session.commit()
        return redirect(url_for('main.index'))
    else:
        flash('Le mot de passe actuel n\'est pas le bon')
        return redirect(url_for('main.password'))

@main.route('/password',methods=['GET', 'POST'])
@login_required
def password():
    return render_template('changepassword.html')
