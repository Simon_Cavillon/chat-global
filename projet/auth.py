from flask import Blueprint, render_template, redirect, url_for, request, flash
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import login_user, logout_user, login_required
from flask_socketio import SocketIO
from .models import UTILISATEUR
from . import db
from datetime import datetime

auth = Blueprint('auth', __name__)

@auth.route('/login')
def login():
    return render_template('login.html')

@auth.route('/login', methods=['POST'])
def login_post():
    email = request.form.get('email')
    password = request.form.get('password')
    remember = True if request.form.get('remember') else False

    user = UTILISATEUR.query.filter_by(email=email).first()

    # Vérifier si l’utilisateur existe réellement
    # Prendre le mot de passe fourni par l’utilisateur, le hacher et le comparer au mot de passe haché dans la base de données
    if not user or not check_password_hash(user.password, password):
        flash('Veuillez vérifier vos informations de connexion et réessayer.')
        return redirect(url_for('auth.login')) # Si l’utilisateur n’existe pas ou si le mot de passe est incorrect, recharger la page

    # Si la vérification ci-dessus réussit, alors nous savons que l’utilisateur a les bons identifiants
    login_user(user, remember=remember)
    return redirect(url_for('main.index'))

@auth.route('/signup')
def signup():
    return render_template('signup.html')

@auth.route('/signup', methods=['POST'])
def signup_post():
    email = request.form.get('email')
    nom = request.form.get('name')
    password = request.form.get('password')

    user = UTILISATEUR.query.filter_by(email=email).first() # Si la ligne de code renvoie un Utilisateur alors celui-ci existe déjà dans la base de donnée

    if user: # Si un utilisateur est trouvé, nous voulons rediriger vers la page d’inscription afin que l’utilisateur puisse réessayer
        flash('Adresse e-mail existe déjà')
        return redirect(url_for('auth.signup'))

    # Créer un nouvel utilisateur avec les données du formulaire. Hacher le mot de passe pour que la version en clair ne soit pas enregistrée.
    new_user = UTILISATEUR(email=email, password=generate_password_hash(password, method='sha256'), nom=nom)

    # Ajouter le nouvel utilisateur à la base de données
    db.session.add(new_user)
    db.session.commit()

    return redirect(url_for('auth.login'))

@auth.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('main.index'))
